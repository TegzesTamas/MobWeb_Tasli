package hu.bme.aut.tasli.database

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.util.Log
import hu.bme.aut.tasli.model.Task

class BackgroundTaskLoaderFragment : Fragment(), LoaderManager.LoaderCallbacks<MutableList<Task>> {
    init {
        Log.d(TAG, "Instance created")
    }

    interface BackgroundTaskDataReceiver {
        fun receiveTasks(tasks: MutableList<Task>)
    }

    private lateinit var receiver: BackgroundTaskDataReceiver

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<MutableList<Task>> {
        Log.d(TAG, "onCreateLoader")
        return TaskLoader(context!!)
    }

    override fun onLoaderReset(loader: Loader<MutableList<Task>>?) {
        Log.d(TAG, "onLoaderReset")
        loader?.reset()
    }

    override fun onLoadFinished(loader: Loader<MutableList<Task>>?, tasks: MutableList<Task>?) {
        Log.d(TAG, "onLoadFinished")
        if (tasks == null) {
            Log.d(TAG, "Null reference returned")
            receiver.receiveTasks(ArrayList())
        } else {
            tasks.forEach { task: Task ->
                Log.d(TAG, task.name)
            }
            receiver.receiveTasks(ArrayList(tasks))
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        receiver = context as BackgroundTaskDataReceiver
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loaderManager.initLoader(0, null, this)
    }

    companion object {
        private val TAG = "BgTaskLdFragment"
    }
}// Required empty public constructor
