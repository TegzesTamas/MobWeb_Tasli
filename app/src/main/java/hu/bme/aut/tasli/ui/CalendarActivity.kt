package hu.bme.aut.tasli.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.CalendarContract
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import hu.bme.aut.tasli.R
import hu.bme.aut.tasli.database.BackgroundCalendarLoaderFragment
import hu.bme.aut.tasli.database.BackgroundTaskLoaderFragment
import hu.bme.aut.tasli.database.onSameDay
import hu.bme.aut.tasli.model.Event
import hu.bme.aut.tasli.model.Task
import kotlinx.android.synthetic.main.activity_calendar.*
import java.util.*

class CalendarActivity : AppCompatActivity(),
        BackgroundCalendarLoaderFragment.BackgroundCalendarDataReceiver,
        BackgroundTaskLoaderFragment.BackgroundTaskDataReceiver {

    companion object {
        const val TAG = "CALENDAR_ACTIVITY"
        const val REQUEST_CODE = 1234
    }


    private var backgroundCalendarLoaderFragment = BackgroundCalendarLoaderFragment()
    private var backgroundTaskLoaderFragment = BackgroundTaskLoaderFragment()

    var tasks: List<Task>? = null
    var events: List<Event>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)
        mcv_main.setOnDateChangedListener { widget, date, selected ->
            val tasksOnThisDay = tasks.orEmpty().filter { task ->
                onSameDay(task.deadline, date.calendar) || onSameDay(task.scheduledTime, date.calendar)
            }

            val endOfTheDay = date.calendar.clone() as Calendar
            endOfTheDay.add(Calendar.DAY_OF_MONTH, 1)
            endOfTheDay.set(Calendar.HOUR, 0)
            endOfTheDay.set(Calendar.MINUTE, 0)
            endOfTheDay.set(Calendar.SECOND, 0)
            endOfTheDay.set(Calendar.MILLISECOND, 0)
            val eventsOnThisDay = events.orEmpty().filter { event ->
                onSameDay(date.calendar, event)
            }
            when (tasksOnThisDay.size + eventsOnThisDay.size) {
                0 -> Snackbar.make(widget, getString(R.string.calendar_activity_message_day_without_task_selected), Snackbar.LENGTH_LONG)
                        .setAction("View Calendar", { _ ->
                            startActivity(Intent(Intent.ACTION_VIEW,
                                    CalendarContract.CONTENT_URI.buildUpon().appendPath("time").appendPath(date.date.time.toString()).build()
                            ))
                        }).show()
                1 -> {
                    if (eventsOnThisDay.isEmpty()) {
                        startActivity(Intent(this@CalendarActivity,
                                TaskDetailsActivity::class.java)
                                .putExtra(TaskDetailsActivity.KEY_TASK_ID,
                                        tasksOnThisDay[0].id))
                    } else {
                        startActivity(Intent(Intent.ACTION_VIEW,
                                CalendarContract.Events.CONTENT_URI.buildUpon().appendPath(eventsOnThisDay[0].id.toString()).build()
                        ))
                    }
                }
                else -> {
                    val builder = AlertDialog.Builder(this@CalendarActivity)
                    val items = ArrayList<String>()
                    items.addAll(tasksOnThisDay.map { task -> task.name })
                    items.addAll(eventsOnThisDay.map { event -> event.title })
                    builder.setTitle(getString(R.string.calendar_activity_pick_task_dialog_title))
                            .setItems(items.toTypedArray(),
                                    { _, which ->
                                        when (which) {
                                            in tasksOnThisDay.indices -> startActivity(Intent(this@CalendarActivity,
                                                    TaskDetailsActivity::class.java)
                                                    .putExtra(TaskDetailsActivity.KEY_TASK_ID,
                                                            tasksOnThisDay[which].id))
                                            else -> {
                                                startActivity(Intent(Intent.ACTION_VIEW,
                                                        CalendarContract.Events.CONTENT_URI.buildUpon().appendPath(eventsOnThisDay[which - tasksOnThisDay.size].id.toString()).build()
                                                ))
                                            }
                                        }

                                    })
                            .setCancelable(true).create().show()
                }
            }
        }


        //not done deadline only
        mcv_main.addDecorator(object : DayViewDecorator {
            override fun shouldDecorate(day: CalendarDay?): Boolean =
                    tasks.orEmpty().any { task ->
                        !task.done
                                && onSameDay(day?.calendar, task.deadline)
                    } && tasks.orEmpty().all { task ->
                        task.done
                                || !onSameDay(day?.calendar, task.scheduledTime)
                    }

            override fun decorate(view: DayViewFacade?) {
                view?.setBackgroundDrawable(ContextCompat.getDrawable(this@CalendarActivity, R.drawable.deadline_circle)!!)
            }
        })

        //not done schedule, no deadline
        mcv_main.addDecorator(object : DayViewDecorator {
            override fun shouldDecorate(day: CalendarDay?): Boolean =
                    tasks.orEmpty().any { task ->
                        !task.done
                                && onSameDay(day?.calendar, task.scheduledTime)
                    } && tasks.orEmpty().all { task ->
                        !onSameDay(day?.calendar, task.deadline)
                    }

            override fun decorate(view: DayViewFacade?) {
                view?.setBackgroundDrawable(ContextCompat.getDrawable(this@CalendarActivity, R.drawable.schedule_circle)!!)
            }
        })

        //not done schedule, done deadline
        mcv_main.addDecorator(object : DayViewDecorator {
            override fun shouldDecorate(day: CalendarDay?): Boolean =
                    tasks.orEmpty().any { task ->
                        !task.done && onSameDay(day?.calendar, task.scheduledTime)
                    } && tasks.orEmpty().any { task ->
                        task.done && onSameDay(day?.calendar, task.deadline)
                    } && tasks.orEmpty().all { task ->
                        task.done || !onSameDay(day?.calendar, task.deadline)
                    }

            override fun decorate(view: DayViewFacade?) {
                view?.setBackgroundDrawable(ContextCompat.getDrawable(this@CalendarActivity, R.drawable.schedule_deadline_circle_done)!!)
            }
        })

        //not done schedule, not done deadline
        mcv_main.addDecorator(object : DayViewDecorator {
            override fun shouldDecorate(day: CalendarDay?): Boolean =
                    tasks.orEmpty().any { task ->
                        !task.done && onSameDay(day?.calendar, task.scheduledTime)
                    } && tasks.orEmpty().any { task ->
                        !task.done && onSameDay(day?.calendar, task.deadline)
                    }

            override fun decorate(view: DayViewFacade?) {
                view?.setBackgroundDrawable(ContextCompat.getDrawable(this@CalendarActivity, R.drawable.schedule_deadline_circle)!!)
            }
        })

        //not done deadline
        mcv_main.addDecorator(object : DayViewDecorator {
            override fun shouldDecorate(day: CalendarDay?): Boolean =
                    tasks.orEmpty().any { task ->
                        task.done && onSameDay(day?.calendar, task.deadline)
                    } && tasks.orEmpty().all { task ->
                        task.done || (!onSameDay(day?.calendar, task.deadline) && !onSameDay(day?.calendar, task.scheduledTime))
                    }

            override fun decorate(view: DayViewFacade?) {
                view?.setBackgroundDrawable(ContextCompat.getDrawable(this@CalendarActivity, R.drawable.deadline_circle_done)!!)
            }
        })

        //Event, no task
        mcv_main.addDecorator(object : DayViewDecorator {
            override fun shouldDecorate(day: CalendarDay?): Boolean = events.orEmpty().any { event ->
                onSameDay(day?.calendar, event)
            } && tasks.orEmpty().all { task ->
                !onSameDay(day?.calendar, task.deadline) &&
                        (task.done || !onSameDay(day?.calendar, task.scheduledTime))
            }

            override fun decorate(view: DayViewFacade?) {
                view?.setBackgroundDrawable(ContextCompat.getDrawable(this@CalendarActivity, R.drawable.event_circle)!!)
            }

        })

        requestCalendarPermission()
        supportFragmentManager.beginTransaction().add(backgroundTaskLoaderFragment, "CABackgroundTaskLoaderFragment").commit()
    }

    private var loadingSnackbar: Snackbar? = null
        get() {
            if (field == null) {
                field = Snackbar.make(mcv_main, "Waiting for events to load", Snackbar.LENGTH_INDEFINITE)
            }
            return field
        }

    private fun requestCalendarPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CALENDAR), REQUEST_CODE)
        } else {
            supportFragmentManager.beginTransaction().add(backgroundCalendarLoaderFragment, "BackgroundCalendarLoaderFragment").commit()
            loadingSnackbar?.show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                supportFragmentManager.beginTransaction().add(backgroundCalendarLoaderFragment, "BackgroundCalendarLoaderFragment").commit()
                loadingSnackbar?.show()
            } else {
                Snackbar.make(mcv_main, "Without permission, the events in the calendar will not be visible.", Snackbar.LENGTH_LONG).show()
            }
        }
    }

    override fun receiveEvents(events: MutableList<Event>) {
        Log.d(TAG, "Received events")
        loadingSnackbar?.dismiss()
        this.events = events
        mcv_main.invalidateDecorators()
    }

    override fun receiveTasks(tasks: MutableList<Task>) {
        Log.d(TAG, "Received tasks")
        this.tasks = tasks
        mcv_main.invalidateDecorators()
    }
}
