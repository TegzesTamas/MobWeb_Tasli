package hu.bme.aut.tasli.ui

import hu.bme.aut.tasli.model.Task

interface TaskListElementClickHandler {
    fun taskClicked(task : Task)
}