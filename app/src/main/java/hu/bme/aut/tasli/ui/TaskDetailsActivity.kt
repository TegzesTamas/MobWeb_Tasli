package hu.bme.aut.tasli.ui

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import com.orm.SugarRecord
import hu.bme.aut.tasli.R
import hu.bme.aut.tasli.model.Task
import kotlinx.android.synthetic.main.activity_task_details.*
import java.text.DateFormat
import java.util.*

class TaskDetailsActivity : AppCompatActivity() {
    companion object {
        const val KEY_TASK_ID = "KEY_TASK_ID"
        private const val TAG = "TaskDetailsActivity"
    }

    lateinit var task : Task
    var dirty = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_details)
        val taskId = intent.getLongExtra(KEY_TASK_ID, -1)
        if (taskId != -1L) {
            task = SugarRecord.findById(Task::class.java, taskId)
            dirty = false
        } else {
            task = Task()
            dirty = true
        }

        et_task_name.setText(task.name)
        et_task_name.setOnKeyListener { _, keyCode, _ ->
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                et_task_desc.requestFocus()
            } else{
                false
            }
        }
        et_task_desc.setText(task.description)
        val dateFormat = DateFormat.getDateInstance(DateFormat.LONG)
        val timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
        tv_task_field_deadline_date.text = dateFormat.format(task.deadline.time)
        tv_task_field_deadline_time.text = timeFormat.format(task.deadline.time)
        et_task_duration.setText(String.format(Locale.getDefault(),"%.2f", task.hoursRequired))
        val scheduledTime = task.scheduledTime
        if(scheduledTime != null) {
            tv_task_field_scheduled_date.text = dateFormat.format(scheduledTime.time)
            tv_task_field_scheduled_time.text = timeFormat.format(scheduledTime.time)
        }
        chkbox_task_done.isChecked = task.done
        val calendar = Calendar.getInstance()
        ll_task_field_deadline_date.setOnClickListener{ val datePickerDialog = DatePickerDialog(this@TaskDetailsActivity,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    task.deadline.set(Calendar.YEAR, year)
                    task.deadline.set(Calendar.MONTH, month)
                    task.deadline.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    tv_task_field_deadline_date.text = dateFormat.format(task.deadline.time)
                    dirty = true
                },
                task.deadline.get(Calendar.YEAR), task.deadline.get(Calendar.MONTH),
                task.deadline.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.minDate = calendar.timeInMillis
            datePickerDialog.show()
        }


        ll_task_field_scheduled_date.setOnClickListener{ val datePickerDialog = DatePickerDialog(this@TaskDetailsActivity,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    if (task.scheduledTime == null)
                        task.scheduledTime = Calendar.getInstance()
                    task.scheduledTime!!.set(Calendar.YEAR, year)
                    task.scheduledTime!!.set(Calendar.MONTH, month)
                    task.scheduledTime!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    tv_task_field_scheduled_date.text = dateFormat.format(task.scheduledTime!!.time)
                    tv_task_field_scheduled_time.text = timeFormat.format(task.scheduledTime!!.time)
                    Log.d(TAG, timeFormat.format(task.scheduledTime!!.time))
                    dirty = true
                },
                (task.scheduledTime ?: calendar).get(Calendar.YEAR),
                (task.scheduledTime ?: calendar).get(Calendar.MONTH),
                (task.scheduledTime ?: calendar).get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.minDate = calendar.timeInMillis
            task.hoursRequired = et_task_duration.text.toString().toDoubleOrNull() ?: 0.0
            datePickerDialog.datePicker.maxDate = task.deadline.timeInMillis - task.msRequired
            datePickerDialog.show()
        }

        ll_task_field_deadline_time.setOnClickListener{TimePickerDialog(this@TaskDetailsActivity,
                TimePickerDialog.OnTimeSetListener { _, hourOfDay, min ->
                    task.deadline.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    task.deadline.set(Calendar.MINUTE, min)
                    tv_task_field_deadline_time.text = timeFormat.format(task.deadline.time)
                    dirty = true
                },
                task.deadline.get(Calendar.HOUR_OF_DAY),
                task.deadline.get(Calendar.MINUTE),
                true).show()
        }

        ll_task_field_scheduled_time.setOnClickListener{
            if(task.scheduledTime == null) {
                Snackbar.make(ll_task_field_scheduled_time,R.string.task_details_scheduled_time_clicked_before_date,Snackbar.LENGTH_SHORT).show()
            } else {
                val timePickerDialog = TimePickerDialog(this@TaskDetailsActivity,
                        TimePickerDialog.OnTimeSetListener { _, hourOfDay, min ->
                            if (task.scheduledTime == null) {
                                task.scheduledTime = Calendar.getInstance()
                            }
                            task.scheduledTime!!.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            task.scheduledTime!!.set(Calendar.MINUTE, min)
                            tv_task_field_scheduled_time.text = timeFormat.format(task.scheduledTime!!.time)
                            dirty = true
                        },
                        (task.scheduledTime ?: calendar).get(Calendar.HOUR_OF_DAY),
                        (task.scheduledTime ?: calendar).get(Calendar.MINUTE),
                        true)
                timePickerDialog.show()
            }
        }
        chkbox_task_done.setOnCheckedChangeListener { _, _ -> dirty = true }
        val textWatcher: TextWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                dirty = true
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                dirty = true
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                dirty = true
            }

        }
        et_task_name.addTextChangedListener(textWatcher)
        et_task_desc.addTextChangedListener(textWatcher)
        et_task_duration.addTextChangedListener(textWatcher)
    }

    override fun onBackPressed() = if (dirty) {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setItems(
                arrayOf(
                        getString(R.string.task_details_activity_exit_alert_option_save),
                        getString(R.string.task_details_activity_exit_alert_option_discard),
                        getString(R.string.task_details_activity_exit_alert_option_keep_editing)
                ),
                { dialog, which ->
                    when (which) {
                        0 -> {
                            saveTask()
                            super.onBackPressed()
                        }
                        1 -> super.onBackPressed()
                        2 -> dialog.dismiss()
                    }
                }
        ).setTitle(getString(R.string.task_details_activity_exit_alert_message)).create().show()
    } else {
        super.onBackPressed()
    }

    private fun saveTask(){
        try {
            task.name = et_task_name.text.toString()
            task.description = et_task_desc.text.toString()
            task.done = chkbox_task_done.isChecked
            val durationString = et_task_duration.text.toString()
            val hoursRequired = java.lang.Double.parseDouble(durationString)
            task.hoursRequired = hoursRequired
            task.save()
            dirty = false
        } catch (e : UninitializedPropertyAccessException){
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_task_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save_task -> {
                saveTask()
                Snackbar.make(et_task_desc, getString(R.string.task_details_activity_message_task_saved), Snackbar.LENGTH_SHORT).show()
            }

            R.id.action_delete_task -> {
                task.delete()
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
