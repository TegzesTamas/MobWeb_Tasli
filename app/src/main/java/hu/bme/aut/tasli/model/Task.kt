package hu.bme.aut.tasli.model

import com.orm.SugarRecord
import java.util.*

data class Task(
        var name : String = "",
        var description : String="",
        val deadline : Calendar = Calendar.getInstance(),
        var msRequired : Long = 0,
        var scheduledTime : Calendar? = null,
        var done : Boolean = false,
        var pos : Int = -1
) : SugarRecord(){
    var hoursRequired : Double
        get() = msRequired / 1000.0 / 60 / 60
        set(hoursRequired) {msRequired = (hoursRequired * 60 * 60 * 1000).toLong()}
}