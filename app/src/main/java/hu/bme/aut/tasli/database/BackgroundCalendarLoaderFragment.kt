package hu.bme.aut.tasli.database

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.provider.CalendarContract
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.util.Log
import hu.bme.aut.tasli.model.Event
import java.util.*

class BackgroundCalendarLoaderFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {

    interface BackgroundCalendarDataReceiver {
        fun receiveEvents(events: MutableList<Event>)
    }

    private lateinit var receiver: BackgroundCalendarDataReceiver

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        return CursorLoader(
                activity!!.applicationContext,
                CalendarContract.Events.CONTENT_URI,
                arrayOf(
                        CalendarContract.Events._ID,
                        CalendarContract.Events.TITLE,
                        CalendarContract.Events.DTSTART,
                        CalendarContract.Events.DTEND,
                        CalendarContract.Events.ALL_DAY
                ), "", arrayOf(), "")
    }

    override fun onLoaderReset(loader: Loader<Cursor>?) {
        loader?.reset()
    }


    override fun onLoadFinished(loader: Loader<Cursor>?, data: Cursor?) {
        val events = ArrayList<Event>()
        while (data?.moveToNext() == true) {
            val title = data.getString(data.getColumnIndex(CalendarContract.Events.TITLE))
            val id = data.getInt(data.getColumnIndex(CalendarContract.Events._ID))
            var dtstart = data.getLong(data.getColumnIndex(CalendarContract.Events.DTSTART))
            var dtend = data.getLong(data.getColumnIndex(CalendarContract.Events.DTEND))
            val allDay = data.getInt(data.getColumnIndex(CalendarContract.Events.ALL_DAY)) != 0
            val calStart = Calendar.getInstance()
            calStart.timeInMillis = dtstart
            val calEnd = Calendar.getInstance()
            calEnd.timeInMillis = dtend
            if (allDay) {
                dtstart -= TimeZone.getDefault().rawOffset
                dtend -= TimeZone.getDefault().rawOffset
                if (calStart.get(Calendar.HOUR_OF_DAY) < 12) {
                    calStart.timeInMillis = dtstart
                    calEnd.timeInMillis = dtstart
                } else {
                    calStart.timeInMillis = dtend
                    calEnd.timeInMillis = dtend
                }
                calStart.set(Calendar.HOUR_OF_DAY, 0)
                calStart.set(Calendar.MINUTE, 0)
                calStart.set(Calendar.SECOND, 0)
                calStart.set(Calendar.MILLISECOND, 0)
                calEnd.set(Calendar.HOUR_OF_DAY, 23)
                calEnd.set(Calendar.MINUTE, 59)
                calEnd.set(Calendar.SECOND, 59)
                calEnd.set(Calendar.MILLISECOND, 999)
            } else {
                calStart.timeInMillis = dtstart
                calEnd.timeInMillis = dtend
            }
            events.add(Event(id, title ?: "", calStart, calEnd))

            Log.d(TAG, "Event read : ID = $id\t" +
                    "${calStart.get(Calendar.MONTH)}-" +
                    "${calStart.get(Calendar.DAY_OF_MONTH)} " +
                    "${calStart.get(Calendar.HOUR_OF_DAY)}:" +
                    "${calStart.get(Calendar.MINUTE)}:" +
                    "${calStart.get(Calendar.SECOND)}:" +
                    "${calStart.get(Calendar.MILLISECOND)}, " +
                    "\t" +
                    "${calEnd.get(Calendar.MONTH)}-" +
                    "${calEnd.get(Calendar.DAY_OF_MONTH)} " +
                    "${calEnd.get(Calendar.HOUR_OF_DAY)}:" +
                    "${calEnd.get(Calendar.MINUTE)}:" +
                    "${calEnd.get(Calendar.SECOND)}:" +
                    "${calEnd.get(Calendar.MILLISECOND)}" +
                    "\t$title ")
        }
        receiver.receiveEvents(events)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        receiver = context as BackgroundCalendarDataReceiver
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loaderManager.initLoader(0, null, this)
    }

    companion object {
        private val TAG = "BgCalendarLdFragment"
    }
}// Required empty public constructor
