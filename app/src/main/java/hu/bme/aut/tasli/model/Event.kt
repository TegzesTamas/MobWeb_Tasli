package hu.bme.aut.tasli.model

import java.util.*

data class Event(val id: Int, val title: String, val start: Calendar, val end: Calendar)