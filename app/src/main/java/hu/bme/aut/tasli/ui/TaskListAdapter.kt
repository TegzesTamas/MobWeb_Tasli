package hu.bme.aut.tasli.ui

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import com.orm.SugarRecord
import hu.bme.aut.tasli.R
import hu.bme.aut.tasli.model.Task
import java.text.DateFormat
import java.util.*


class TaskListAdapter(
        private val taskListElementClickHandler: TaskListElementClickHandler) : RecyclerView.Adapter<TaskListAdapter.TaskViewHolder>() {

    private val tasks: MutableList<Task> = ArrayList()

    fun delTask(position: Int) {
        tasks.removeAt(position).delete()
        notifyItemRemoved(position)
    }

    fun update(newTasks: List<Task>) {
        tasks.clear()
        tasks.addAll(newTasks)
        tasks.sortBy(Task::pos)
        notifyDataSetChanged()
    }

    fun deleteAll() {
        tasks.clear()
        SugarRecord.deleteAll(Task::class.java)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TaskViewHolder {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_task_list, parent, false)
        return TaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TaskViewHolder?, position: Int) {
        val dateFormatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT)
        val task = tasks[position]
        holder!!
        holder.tvTaskName.text = task.name
        holder.tvDeadline.text = dateFormatter.format(task.deadline.time)
        val scheduledTime = task.scheduledTime
        holder.tvSchedule.text = if (scheduledTime != null) dateFormatter.format(scheduledTime.time) else ""
        holder.tvDuration.text = String.format(Locale.getDefault(), "%.2f hours", task.hoursRequired)
        holder.chkBoxDone.isChecked = task.done
    }

    override fun getItemCount() = tasks.size

    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTaskName: TextView = itemView.findViewById(R.id.tv_tasklistelement_taskname)
        val tvDeadline: TextView = itemView.findViewById(R.id.tv_tasklistelement_deadline)
        val tvDuration: TextView = itemView.findViewById(R.id.tv_tasklistelement_duration)
        val tvSchedule: TextView = itemView.findViewById(R.id.tv_tasklistelement_schedule)
        val btnDelTask: ImageButton = itemView.findViewById(R.id.btn_tasklistelement_deltask)
        val chkBoxDone: CheckBox = itemView.findViewById(R.id.chkBox_tasklistelement_done)

        init {
            itemView.setOnClickListener { taskListElementClickHandler.taskClicked(tasks[adapterPosition]) }
            btnDelTask.setOnClickListener { this@TaskListAdapter.delTask(adapterPosition) }
            chkBoxDone.setOnCheckedChangeListener{_, isChecked -> tasks[adapterPosition].done = isChecked; tasks[adapterPosition].save() }
        }
    }

    inner class TouchHelperCallback : ItemTouchHelper.Callback() {

        override fun isLongPressDragEnabled() = true
        override fun isItemViewSwipeEnabled() = true

        override fun getMovementFlags(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
            val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
            val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
            return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
        }

        override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
            val fromPosition = viewHolder!!.adapterPosition
            val toPosition = target!!.adapterPosition
            if (fromPosition < toPosition) {
                for (i in fromPosition until toPosition) {
                    Collections.swap(tasks, i, i + 1)
                    tasks[i + 1].pos = i + 1
                    tasks[i].pos = i
                    tasks[i].save()
                }
            } else {
                for (i in fromPosition downTo toPosition + 1) {
                    Collections.swap(tasks, i, i - 1)
                    tasks[i - 1].pos = i - 1
                    tasks[i].pos = i
                    tasks[i].save()
                }
            }
            tasks[toPosition].save()
            notifyItemMoved(fromPosition, toPosition)
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
            delTask(viewHolder!!.adapterPosition)
        }

    }
}