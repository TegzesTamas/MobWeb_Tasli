package hu.bme.aut.tasli.database

import android.content.Context
import android.support.v4.content.AsyncTaskLoader
import android.util.Log
import com.orm.SugarRecord
import hu.bme.aut.tasli.model.Task


class TaskLoader(context: Context) : AsyncTaskLoader<MutableList<Task>>(context) {
    init {
        Log.d("TaskLoader", "Instance created")
    }

    companion object {
        const val TAG = "TaskLoader"
    }

    override fun loadInBackground(): MutableList<Task> {
        Log.d("TaskLoader", "loadInBackground")
        val tasks = SugarRecord.listAll(Task::class.java)
        Log.d("TaskLoader", "tasks loaded")
        return tasks
    }

    override fun onStartLoading() {
        forceLoad()
    }

}