package hu.bme.aut.tasli.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.MenuItem
import hu.bme.aut.tasli.R
import hu.bme.aut.tasli.database.BackgroundTaskLoaderFragment
import hu.bme.aut.tasli.model.Task
import kotlinx.android.synthetic.main.activity_task_list.*
import kotlinx.android.synthetic.main.content_task_list.*

class TaskListActivity : AppCompatActivity(), TaskListElementClickHandler, BackgroundTaskLoaderFragment.BackgroundTaskDataReceiver {

    override fun receiveTasks(tasks: MutableList<Task>) {
        adapter.update(tasks)
    }

    private lateinit var adapter : TaskListAdapter
    override fun taskClicked(task: Task) {
        val intent = Intent(this,TaskDetailsActivity::class.java)
        intent.putExtra(TaskDetailsActivity.KEY_TASK_ID, task.id)
        startActivity(intent)
    }

    private val backgroundTaskLoaderFragment = BackgroundTaskLoaderFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_list)
        setSupportActionBar(toolbar)
        adapter = TaskListAdapter(this)
        initRecyclerView()
        fab.setOnClickListener { _ ->
            startActivity(Intent(this,TaskDetailsActivity::class.java))
        }
        supportFragmentManager.beginTransaction().add(backgroundTaskLoaderFragment, "BackgroundTaskLoaderFragment").commit()
    }

    override fun onResume() {
        super.onResume()

//        BackgroundTaskLoader(adapter).execute()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_task_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_delete_all) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.alertdialog_delete_all_tasks_title)
                    .setMessage(R.string.alertdialog_delete_all_tasks_message)
                    .setCancelable(true)
                    .setNegativeButton(R.string.alertdialog_delete_all_tasks_button_cancel, { _, _ -> })
                    .setPositiveButton(R.string.alertdialog_delete_all_tasks_button_accept, { _, _ -> adapter.deleteAll() })
                    .create().show()
            return true
        }
        if(item.itemId == R.id.action_calendar){
            startActivity(Intent(this, CalendarActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initRecyclerView(){
        rv_tasklist.layoutManager = LinearLayoutManager(this)
        rv_tasklist.adapter = adapter
        ItemTouchHelper(adapter.TouchHelperCallback()).attachToRecyclerView(rv_tasklist)
    }


}
