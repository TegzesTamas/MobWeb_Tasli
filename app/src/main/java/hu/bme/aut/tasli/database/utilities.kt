package hu.bme.aut.tasli.database

import hu.bme.aut.tasli.model.Event
import java.util.*


fun onSameDay(calendar1: Calendar?, calendar2: Calendar?): Boolean =
        calendar1 != null && calendar2 != null &&
                calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)

fun onSameDay(calendar: Calendar?, event: Event?): Boolean = (calendar != null && event != null
        && event.start.timeInMillis < calendar.timeInMillis + 24 * 60 * 60 * 1000
        && event.end.timeInMillis > calendar.timeInMillis)
